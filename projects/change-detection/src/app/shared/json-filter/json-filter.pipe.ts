import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jsonFilter',
  pure: false
})
export class JsonFilterPipe implements PipeTransform {

  transform(value: any, filter: any): any {
    return JSON.stringify(value, filter);
  }

}
