import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {JsonFilterPipe} from "./json-filter/json-filter.pipe";

@NgModule({
  declarations: [JsonFilterPipe],
  imports: [
    CommonModule
  ],
  exports: [JsonFilterPipe]
})
export class SharedModule {
}
