export function log(target: Object,
                    propertyName: string,
                    propertyDesciptor: PropertyDescriptor) {
  return {
    value: function (...args: any[]): any {
      const result = propertyDesciptor.value.apply(this, args);

      console.log(`Method ${propertyName} was called. Return: ${result}`);
    }
  };
}
