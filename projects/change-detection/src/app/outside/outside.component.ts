import {Component, ViewChild} from '@angular/core';
import {ChildOneComponent} from "./child-one/child-one.component";

@Component({
  selector: 'app-outside',
  templateUrl: './outside.component.html',
  styleUrls: ['./outside.component.scss'],
})
export class OutsideComponent {
  @ViewChild(ChildOneComponent, {static: true})
  childOneComponent: ChildOneComponent;
}
