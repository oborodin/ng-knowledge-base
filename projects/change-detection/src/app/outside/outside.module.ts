import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OutsideComponent } from './outside.component';
import { ChildOneComponent } from './child-one/child-one.component';
import {SharedModule} from "../shared/shared.module";



@NgModule({
  declarations: [OutsideComponent, ChildOneComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [OutsideComponent]
})
export class OutsideModule { }
