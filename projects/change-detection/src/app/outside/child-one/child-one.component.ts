import {Component, OnInit, NgZone} from '@angular/core';
import {log} from "../../shared/log.functon";

@Component({
  selector: 'app-child-one',
  templateUrl: './child-one.component.html',
  styleUrls: ['./child-one.component.scss'],
})
export class ChildOneComponent implements OnInit {
  count = 0;

  constructor(private zone: NgZone) {
  }

  ngOnInit() {
  }

  @log
  run() {
    this.zone.runOutsideAngular(() => {
    });
  }
}
