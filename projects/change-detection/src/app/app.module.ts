import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {OnPushModule} from "./on-push/on-push.module";
import {DetachModule} from "./detach/detach.module";
import {SharedModule} from "./shared/shared.module";
import {OutsideModule} from "./outside/outside.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OnPushModule,
    DetachModule,
    SharedModule,
    OutsideModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
