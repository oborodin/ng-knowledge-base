import {Component, ViewChild} from '@angular/core';
import {ChildOneComponent} from "./child-one/child-one.component";
import {log} from "../shared/log.functon";

@Component({
  selector: 'app-on-push',
  templateUrl: './on-push.component.html',
  styleUrls: ['./on-push.component.scss'],
})
export class OnPushComponent {
  @ViewChild(ChildOneComponent, {static: true})
  childOneComponent: ChildOneComponent;

  user = {name: 'name'};

  title = 'change-detection';

  @log
  addForChild() {
    this.childOneComponent.add()
  }

  @log
  mutableChangeUser() {
    this.user.name = 'mutable name';
  }

  @log
  immutableChangeUser() {
    this.user = {...this.user, name: 'immutable name'};
  }
}
