import {Component, ChangeDetectionStrategy, Input, ChangeDetectorRef} from '@angular/core';
import {log} from "../../shared/log.functon";

@Component({
  selector: 'app-child-one',
  templateUrl: './child-one.component.html',
  styleUrls: ['./child-one.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChildOneComponent {

  count = 1;

  @Input()
  user = {name: 'name'};

  constructor(private cdr: ChangeDetectorRef) {
  }

  @log
  add() {
    this.count++;
    console.log(this.count);
    return this.count;
  }

  @log
  addByTimeout() {
    setTimeout(()=>{
      this.add();
    });
  }

  @log
  addByTimeoutWithChecking() {
    setTimeout(()=>{
      this.add();
      this.cdr.markForCheck();
    });
  }
}
