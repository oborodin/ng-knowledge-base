import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OnPushComponent} from "./on-push.component";
import {ChildOneComponent} from './child-one/child-one.component';
import {SharedModule} from "../shared/shared.module";


@NgModule({
  declarations: [
    OnPushComponent,
    ChildOneComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    OnPushComponent,
  ]
})
export class OnPushModule {
}
