import {Component, ViewChild} from '@angular/core';
import {ChildOneComponent} from "./child-one/child-one.component";

@Component({
  selector: 'app-detach',
  templateUrl: './detach.component.html',
  styleUrls: ['./detach.component.scss'],
})
export class DetachComponent {
  @ViewChild(ChildOneComponent, {static: true})
  childOneComponent: ChildOneComponent;
}
