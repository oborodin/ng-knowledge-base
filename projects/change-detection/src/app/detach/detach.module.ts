import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetachComponent } from './detach.component';
import {SharedModule} from "../shared/shared.module";
import { ChildOneComponent } from './child-one/child-one.component';



@NgModule({
  declarations: [DetachComponent, ChildOneComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [DetachComponent]
})
export class DetachModule { }
