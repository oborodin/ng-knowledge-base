import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {log} from "../../shared/log.functon";

@Component({
  selector: 'app-child-one',
  templateUrl: './child-one.component.html',
  styleUrls: ['./child-one.component.scss'],
})
export class ChildOneComponent {
  count = 0;

  constructor(private cdr: ChangeDetectorRef) {
  }

  @log
  add() {
    this.count++;
  }

  @log
  remove() {
    this.count--;
  }

  @log
  detach() {
    this.cdr.detach();
  }

  @log
  reattach() {
    this.cdr.reattach();
  }

  @log
  detectChanges() {
    this.cdr.detectChanges();
  }

  @log
  markForCheck() {
    this.cdr.markForCheck();
  }
}
